# #!/usr/bin/env bash
echo -e "\n\n\n\n\n"
echo "================================================================================================================="
echo "Starting configuring Ubuntu Server"
echo "================================================================================================================="
echo -e "\n\n\n\n\n"
sleep 5

echo -e "\n\n\n\n================================================================================================================="
echo "Update & upgrade"
sleep 2
apt-get update -y
apt-get upgrade -y

#set right timezone
echo -e "\n\n\n\n================================================================================================================="
echo "Set right timezone"
sleep 2
timedatectl set-timezone Europe/Kiev

apt-get install -y ntp
apt-get install -y ntpdate
ntpdate -s ntp.ubuntu.com

echo -e "\n\n\n\n================================================================================================================="
echo "Install JRE/JDK"
sleep 2
apt-get install -y default-jre

echo -e "\n\n\n\n================================================================================================================="
echo "Install RabbitMQ"
sleep 2
mkdir /var/lib/rabbitmq
apt-get install -y rabbitmq-server
rabbitmq-plugins enable rabbitmq_management
touch /etc/rabbitmq/rabbitmq.config
echo "[{rabbit, [{loopback_users, []}]}]." > /etc/rabbitmq/rabbitmq.config

echo -e "\n\n\n\n================================================================================================================="
echo "Install MC"
sleep 2
apt-get install -y mc

debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

echo -e "\n\n\n\n================================================================================================================="
echo "Install Apache"
sleep 2
apt-get install -y apache2 apache2-dev php5
# Change apache to run as vagrant:vagrant
sed -i s/www-data/vagrant/ /etc/apache2/envvars

# Fix apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
echo "ServerName localhost" | tee /etc/apache2/conf-available/fqdn.conf
a2enconf fqdn

#Enable apache modules
a2enmod rewrite auth_digest authn_anon authn_dbm authz_owner authz_groupfile authz_dbm ldap authnz_ldap include ext_filter mime_magic expires headers usertrack setenvif dav status info dav_fs vhost_alias actions speling userdir alias substitute proxy proxy_balancer proxy_ftp proxy_http proxy_ajp proxy_connect cache suexec cgi

echo -e "\n\n\n\n================================================================================================================="
echo "Install Nginx"
sleep 2
apt-get install nginx -y
update-rc.d -f nginx disable

echo -e "\n\n\n\n================================================================================================================="
echo "Install useful soft"
sleep 2
apt-get install -y vim git zip unzip curl wget memcached imagemagick python-software-properties software-properties-common

echo -e "\n\n\n\n================================================================================================================="
echo "Install MySql"
sleep 2
apt-get install -y mysql-client mysql-server

echo -e "\n\n\n\n================================================================================================================="
echo "Install MongoDB Community Edition"
#https://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/
#https://rohan-paul.github.io/mongodb_in_ubuntu/2015/09/03/How_to_Install_MongoDB_Iin_Ubuntu-15.04.html
sleep 2
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
apt-get update
apt-get install -y mongodb-org
replace "  bindIp: 127.0.0.1" "# bindIp: 127.0.0.1" -- /etc/mongod.conf
echo -e "[Unit]\nDescription=High-performance, schema-free document-oriented database\nDocumentation=man:mongod(1)\nAfter=network.target\n\n[Service]\nType=forking\nUser=mongodb\nGroup=mongodb\nRuntimeDirectory=mongod\nPIDFile=/var/run/mongod/mongod.pid\nExecStart=/usr/bin/mongod -f /etc/mongod.conf --pidfilepath /var/run/mongod/mongod.pid --fork\nTimeoutStopSec=5\nKillMode=mixed\n\n[Install]\nWantedBy=multi-user.target" >> /lib/systemd/system/mongod.service
systemctl enable mongod.service

echo -e "\n\n\n\n================================================================================================================="
echo "Install PHP5 & php modules"
sleep 2
apt-get install -y snmp-mibs-downloader
apt-get install -y php5
apt-get install -y php5-mysql php5-memcached php5-xdebug php5-curl php5-json php5-sqlite php5-mcrypt php5-snmp php5-gd

#curl -sS https://getcomposer.org/installer | php mv composer.phar /usr/local/bin/composer

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

#PHPUnit
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit

echo -e "\n\n\n\n================================================================================================================="
echo "Install NodeJS"
sleep 2
apt-key adv --keyserver keyserver.ubuntu.com --recv 68576280
apt-add-repository "deb https://deb.nodesource.com/node_5.x $(lsb_release -sc) main"
apt-get update
apt-get install -y nodejs
npm install npm -g

echo -e "\n\n\n\n================================================================================================================="
echo "Install NodeJS Http server"
sleep 2
npm install -g http-server

echo -e "\n\n\n\n================================================================================================================="
echo "Install Mongo Express"
sleep 2
npm install -g mongo-express
cp /usr/lib/node_modules/mongo-express/config.default.js /usr/lib/node_modules/mongo-express/config.js
replace "db:       'db'" "db:       'local'" -- /usr/lib/node_modules/mongo-express/config.js
replace "password: 'pass'" "//password: 'pass'" -- /usr/lib/node_modules/mongo-express/config.js
replace "username: 'admin'" "//username: 'admin'" -- /usr/lib/node_modules/mongo-express/config.js
replace "host:             process.env.VCAP_APP_HOST                 || 'localhost'," "host:             process.env.VCAP_APP_HOST                 || '0.0.0.0'," -- /usr/lib/node_modules/mongo-express/config.js

echo -e "\n\n\n\n================================================================================================================="
echo "Install Screen"
sleep 2
apt-get install -y screen

echo -e "\n\n\n\n================================================================================================================="
echo "Install Ruby on Rails"
sleep 2
apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev rbenv

cd
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc


exec $SHELL <<EOF1

git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL <<EOF2

git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash

rbenv install 2.2.4
rbenv global 2.2.4
ruby -v

echo "gem: --no-ri --no-rdoc" > ~/.gemrc
gem install bundler
gem install rails -v 4.2.6
rbenv rehash

echo -e "\n\n\n\n================================================================================================================="
echo "Config Apache2 with Ruby on Rails"
sleep 2
gem install passenger
passenger-install-apache2-module --auto

echo -e "\n\n\n\n================================================================================================================="
echo "Install yeoman.io & generators"
sleep 2
npm install -g yo grunt-cli gulp gulp-cli bower generator-karma generator-angular generator-gulp-angular generator-angular-fullstack generator-angular2 generator-meteor
gem install compass

echo -e "\n\n\n\n================================================================================================================="
echo "Fix Ubuntu issues"
sleep 2
#Fix stdin: is not a tty
sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile

echo -e "\n\n\n\n================================================================================================================="
echo "Update system"
sleep 2
apt-get update -y

EOF2
EOF1
